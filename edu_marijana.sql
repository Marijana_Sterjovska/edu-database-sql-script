
CREATE DATABASE educationcenter;

CREATE TABLE roles (
	id INT PRIMARY KEY,
	name VARCHAR(20) NOT NULL UNIQUE
);

INSERT INTO roles(id, name)
	VALUES (1,'Instructor');

INSERT INTO roles(id, name)
	VALUES (2,'Student');
	
--Табелата  cities можеби беше доволна доколку имаме студенти само од Македонија 
--но зедов во предвид дека можеби понатаму би имало онлајн курсеви кои би ги посетувале и студенти од други држави

CREATE TABLE countries(
	id SERIAL,
	name VARCHAR(30) UNIQUE NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO countries(name)
	VALUES ('Macedonia');

CREATE TABLE cities(
	id SERIAL,
	name VARCHAR(30) UNIQUE NOT NULL,
	country_id INT NOT NULL REFERENCES countries(id) ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY KEY(id)
);

INSERT INTO cities(name, country_id)
	VALUES ('Bitola', 1);

INSERT INTO cities(name, country_id)
	VALUES ('Prilep', 1);
	
INSERT INTO cities(name, country_id)
	VALUES ('Resen', 1);
	
--Во табелата contacts ми се внесени сите личности кои учествуваат во курсевите, на кои подоцна ќе им ја доделам нивната “улога” во курсот. 
--Истата личност да може да има повеќе улоги (да може да биде предавач на еден курс, а слушател на друг курс) 

CREATE TABLE contacts (
	id SERIAL,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(100) NOT NULL,
	email VARCHAR(100) NOT NULL UNIQUE,
	phone VARCHAR(40) NOT NULL,
	city_id INT NOT NULL 
		REFERENCES cities (id) 
		ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY KEY (id)
);
	
INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Nikola', 'Nikolovski', 'nikolan@gmail.com', '077-207-368', 1);
	
INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Petko', 'Petkovski', 'petko@gmail.com', '075-606-044', 1);
	
INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Marija', 'Petroska', 'marijap@gmail.com', '071-607-369', 2);

INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Snezana', 'Ilievska', 'snezili@yahoo.com', '070-252-693', 3);

INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Stefan', 'Stefanov', 'stefan80@yahoo.com', '076-538-190', 1);

INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Mile', 'Koleski', 'koleskim@gmail.com', '077-589-576', 2);

INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Mihaela', 'Kostovska', 'mihaela96@gmail.com', '071-650-487', 1);
	
INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Dragan', 'Draganov', 'dragidr@yahoo.com', '077-565-603', 1);
	
INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Aleksandar', 'Spasevski', 'acesp99@gmail.com', '070-207-501', 1);
	
INSERT INTO contacts(first_name, last_name, email, phone, city_id)
	VALUES ('Katerina', 'Mitrevska', 'katem83@gmail.com', '075-543-842', 1);

CREATE TABLE users(
		id SERIAL,
		username VARCHAR(20) NOT NULL UNIQUE,
		password VARCHAR(40) NOT NULL,
		contact_id INT NOT NULL
			REFERENCES contacts (id)
			ON DELETE RESTRICT ON UPDATE CASCADE,
		create_timestamp TIMESTAMP NOT NULL DEFAULT now(),
		enabled BOOLEAN NOT NULL DEFAULT true,
		PRIMARY KEY (id)
	);

INSERT INTO users(username, password, contact_id)
	VALUES ('nikolan', 'n1278', 1);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('petkopetko', 'm1004', 2);

INSERT INTO users(username, password, contact_id)
	VALUES ('marijapet', 'marp6879', 3);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('snez91', 'sili91', 4);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('stefanovs', 'stefs80', 5);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('milekoleski', 'milek0486', 6);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('mkostovska', 'mihkos96', 7);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('dragidragi', 'dragce591', 8);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('acespasevski', 'ace99s', 9);
	
INSERT INTO users(username, password, contact_id)
	VALUES ('katemit', 'katebt83', 10);

--Во оваа табела се сите курсеви кои се изведуваат во едукативниот центар
 	
CREATE TABLE courses (
	id SERIAL,
	title VARCHAR(150) NOT NULL,
	duration_hours INT NOT NULL,
	price_den DECIMAL(8,2) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO courses(title, duration_hours, price_den)
	VALUES ('Full Stack with Java', 200, 85000);
	
INSERT INTO courses(title, duration_hours, price_den)
	VALUES ('Software testing', 48, 30500);
	
INSERT INTO courses(title, duration_hours, price_den)
	VALUES ('Website development with HTML, CSS and JavaScript', 24, 16000);
	
INSERT INTO courses(title, duration_hours, price_den)
	VALUES ('Data Management with Python', 64, 40000);

/*Во табелата courses_shedule ставам распоред на курсеви за кои има формирано групи и се знае кога ќе започнат. 
Истите курсеви можат да се повторуваат повеќе пати за различни групи. */

CREATE TABLE courses_schedule (
	id SERIAL,
	course_id INT NOT NULL
		REFERENCES courses (id)
		ON DELETE RESTRICT ON UPDATE CASCADE,
	start_date date NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO courses_schedule(course_id, start_date)
	VALUES (1, '2020-12-10');
	
INSERT INTO courses_schedule(course_id, start_date)
	VALUES (2, '2021-01-25');
	
INSERT INTO courses_schedule(course_id, start_date)
	VALUES (3, '2021-02-01');
	
INSERT INTO courses_schedule(course_id, start_date)
	VALUES (1, '2021-02-20');

/*Во табелата participants се ставаат учесниците во одреден курс за кој е формирана група (или веќе започнал) . 
Како учесници во одреден курс личностите имаат улога на инструктор или на студент за тој курс.
Одреден курс може  да се повторува повеќе пати за различни групи, и истиот може да биде со исти, но исто така и со изменети предавачи(инструктори).
Одреден курс може да биде предаван од повеќе предавачи истовремено. */


CREATE TABLE participants (
	course_schedule_id INT NOT NULL
		REFERENCES courses_schedule (id)
		ON DELETE RESTRICT ON UPDATE CASCADE,
	contact_id INT NOT NULL
		REFERENCES contacts (id)
		ON DELETE RESTRICT ON UPDATE CASCADE,
	role_id INT NOT NULL
		REFERENCES roles (id)
		ON DELETE RESTRICT ON UPDATE CASCADE
);
create unique index c_participant on participants (course_schedule_id, contact_id);

INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (1, 1, 1);
	
INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (1, 2, 2);

INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (1, 3, 2);
	
INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (2, 8, 1);

INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (2, 9, 1);	

INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (2, 7, 2);
	
INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (2, 1, 2);
	
INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (2, 2, 2);

INSERT INTO participants(course_schedule_id, contact_id, role_id)
	VALUES (4, 9, 1);		

--Во табелата Classrooms ги имам ставено училниците во кои се одржуваат курсевите.

CREATE TABLE classrooms(
	id SERIAL NOT NULL,
	location VARCHAR(150) NOT NULL,
	capacity INT NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO classrooms(location, capacity)
	VALUES ('Biznis centar I kat', 8);
	
INSERT INTO classrooms(location, capacity)
	VALUES ('Biznis centar II kat', 10);
	
INSERT INTO classrooms(location, capacity)
	VALUES ('Zoom platform ', 30);

--Во табелата week_days ги зачував деновите во неделата, ги користам во табелата lectures_schedule 

CREATE TABLE week_days(
	id SERIAL NOT NULL,
	day VARCHAR(30)UNIQUE NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO week_days(day)
	VALUES ('Sunday');
	
INSERT INTO week_days(day)
	VALUES ('Monday');
	
INSERT INTO week_days(day)
	VALUES ('Tuesday');
	
INSERT INTO week_days(day)
	VALUES ('Wednesday');
	
INSERT INTO week_days(day)
	VALUES ('Thursday');
	
INSERT INTO week_days(day)
	VALUES ('Friday');
	
INSERT INTO week_days(day)
	VALUES ('Saturday');

/*Во табелата lectures_schedule го имам направено распоредот на одржување на курсевите: 
Во која училница се врши предавањето на тој курс, во кои денови од неделата и во кое време.*/

CREATE TABLE lectures_schedule (
	course_schedule_id INT NOT NULL
		REFERENCES courses_schedule (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	week_day_id INT NOT NULL
		REFERENCES week_days (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	classroom_id INT NOT NULL
		REFERENCES classrooms (id)ON DELETE RESTRICT ON UPDATE CASCADE,
	start_time TIME NOT NULL,
	end_time TIME NOT NULL,
	primary key (course_schedule_id, week_day_id)
);

INSERT INTO lectures_schedule(course_schedule_id, week_day_id, classroom_id, start_time, end_time)
	VALUES (1, 2, 3, '16:00', '18:00');

INSERT INTO lectures_schedule(course_schedule_id, week_day_id, classroom_id, start_time, end_time)
	VALUES (1, 3, 3, '16:00', '18:00');
	
INSERT INTO lectures_schedule(course_schedule_id, week_day_id, classroom_id, start_time, end_time)
	VALUES (1, 4, 3, '16:00', '18:00');
	
INSERT INTO lectures_schedule(course_schedule_id, week_day_id, classroom_id, start_time, end_time)
	VALUES (1, 5, 3, '16:00', '18:00');

INSERT INTO lectures_schedule(course_schedule_id, week_day_id, classroom_id, start_time, end_time)
	VALUES (1, 6, 3, '16:00', '18:00');

	
CREATE TABLE payment_methods(
	id SERIAL NOT NULL,
	name VARCHAR(20) NOT NULL,
	PRIMARY KEY (id)
);

INSERT INTO payment_methods(name)
	VALUES ('Visa card');
	
INSERT INTO payment_methods(name)
	VALUES ('Mastercard');
	
INSERT INTO payment_methods(name)
	VALUES ('PayPal');
	
INSERT INTO payment_methods(name)
	VALUES ('Bank transfer');

--Во табелата сакав да направам едноставна евиденција на плаќањата за одреден курс од страна на студентот.

CREATE TABLE payments(
	id SERIAL NOT NULL,
	contacts_id INT NOT NULL
		REFERENCES contacts (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	course_schedule_id INT NOT NULL
		REFERENCES courses_schedule (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	payment_date DATE NOT NULL,
	amount DECIMAL(8,2) NOT NULL,
	payment_method_id INT NOT NULL
		REFERENCES payment_methods (id) ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY KEY (id)
);

INSERT INTO payments(contacts_id, course_schedule_id, payment_date, amount, payment_method_id )
	VALUES (2, 1, '2020-12-10', 10000, 1);
	
INSERT INTO payments(contacts_id, course_schedule_id, payment_date, amount, payment_method_id )
	VALUES (3, 1, '2020-12-25', 20000, 3);
	
INSERT INTO payments(contacts_id, course_schedule_id, payment_date, amount, payment_method_id )
	VALUES (2, 1, '2021-01-05', 15000, 1);	

CREATE TABLE exam_places(
	id SERIAL NOT NULL,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO exam_places(name)
	VALUES ('Fynderfy platform');

--Во табелата exams внесуваме основни податоци за одредени тестови кои би ги полагале студентите. 
	
CREATE TABLE exams(
	id SERIAL NOT NULL,
	name VARCHAR(150) NOT NULL,
	questions INT NOT NULL,
	duration_min INT NOT NULL,
	exam_place_id INT NOT NULL
		REFERENCES  exam_places(id) ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY KEY(id)
);

INSERT INTO exams(name, questions, duration_min, exam_place_id)
	VALUES ('Full Stack Programing with Java', 40, 30, 1);

--Во табелата taken_exams се евидентираат студентите кои полагале одреден тест и резултатите кои ги постигнале на тој тест.	

CREATE TABLE taken_exams(
	contact_id INT NOT NULL
		REFERENCES  contacts(id) ON DELETE RESTRICT ON UPDATE CASCADE,
	exam_id INT NOT NULL
		REFERENCES  exams(id) ON DELETE RESTRICT ON UPDATE CASCADE,
	time_taken VARCHAR(20) NOT NULL,
	result_percents INT NOT NULL,
	correct_quest INT NOT NULL,
	passed BOOLEAN NOT NULL
);
create unique index c_exam on taken_exams (contact_id, exam_id);

INSERT INTO taken_exams(contact_id, exam_id, time_taken, result_percents, correct_quest, passed)
	VALUES (2, 1, '24m17s', 82, 41, true);
	
INSERT INTO taken_exams(contact_id, exam_id, time_taken, result_percents, correct_quest, passed)
	VALUES (3, 1, '29m02s', 72, 36, true);


--овај join ги дава контактите со градовите од кои доаѓаат. Контактите се подредени прво по градот од кој што доаѓаат, па по contact_id

SELECT c.id, c.first_name, c.last_name, c.email, c. phone, cities.name as city
FROM contacts AS c 
FULL OUTER JOIN cities on c.city_id = cities.id
ORDER BY cities.name, c.id;

--ovaj join ги дава курсевите кои започнале да се одржуваат подредени по id, заедно со датата на започнување на курсот и распоредот на одржување на часовите.

SELECT c.id, c.title, c.duration_hours, c.price_den, 
       cs.start_date, days.day,rooms.location, rooms.capacity,ls.start_time, ls.end_time 
FROM courses AS c
INNER JOIN courses_schedule AS cs on c.id = cs.id
INNER JOIN lectures_schedule AS ls on cs.id = ls.course_schedule_id
INNER JOIN week_days AS days on days.id = ls.week_day_id
INNER JOIN classrooms AS rooms on rooms.id = ls.classroom_id
ORDER BY c.id;

/*овај join ги дава името на курсот, датум на започнување на тој курс, кои се учесниците на курсот, од кој град и во која улога(предавач или студент на тој курс), 
подредени се по датум на започнување на курсот 

Овде можи најдобро да се види дека одреден курс може  да се повторува повеќе пати за различни групи, 
и истиот може да биде со исти, но исто така и со изменети предавачи(инструктори).

пр. За курсот со id = 1 (Full Stack with Java), на курсот кој започнал на 10.12.2020 во улога на инструктор е Никола Николовски, 
а на истиот курс кој се повторува започнувајки од 20.20.2021 инструктор е Александар Спасевски со ид=9.

Одреден курс може да биде предаван од повеќе предавачи истовремено.
Пр. на курсот со ид = 2 (Software testing), како предавачи се јавуваат 2 инструктори: Драган Драганов и Александар Спасовски.*/
 
SELECT c.id, c.title, cs.start_date,cont.first_name, cont.last_name, city.name AS city, r.name AS role
FROM courses AS c
INNER JOIN courses_schedule AS cs on c.id = cs.course_id
INNER JOIN participants AS p on cs.id = p.course_schedule_id
INNER JOIN roles AS r on r.id = p.role_id
INNER JOIN contacts AS cont on cont.id = p.contact_id
INNER JOIN cities AS city on city.id = cont.city_id
ORDER BY cs.start_date;
  
/*овај join ги дава името и презимето на студентите кои полагале одреден тест, името на тестот и успехот кој го постигнале на истиот 
подредени се по остварениот резултат започнувајки од најдобриот */

SELECT cont.first_name, cont.last_name, exams.name AS exam_name, 
       te.time_taken, te.result_percents, te.correct_quest, te.passed
FROM taken_exams AS te
INNER JOIN contacts AS cont on cont.id=te.contact_id
INNER JOIN exams on exams.id=te.exam_id
ORDER BY te.result_percents DESC;  
  
-- овај join ги дава уплатите за одреден курс , кој студент која сума ја платил за кој курс, датум на уплата и начин на кој ја извршил уплатата.

SELECT cont.first_name, cont.last_name, c.title as course_title, p.payment_date, 
       p.amount , pm.name AS payment_method
FROM payments AS p 
INNER JOIN courses_schedule AS cs on cs.id = p.course_schedule_id
INNER JOIN contacts AS cont on cont.id = p.contacts_id
INNER JOIN courses AS c on c.id = cs.course_id
INNER JOIN payment_methods AS pm on pm.id = p.payment_method_id
ORDER BY cont.id;

-- ја дава вкупната уплатена сума од секој контакт поединечно

select cont.first_name, cont.last_name, SUM(p.amount)
from contacts as cont,  payments AS p
where cont.id = p.contacts_id
GROUP by cont.first_name, cont.last_name; 

--овај join ни ги прикажува сите информации за курсевите, учесниците во истите, почеток на курсеви и распоредот на започнатите курсеви

SELECT c.id, c.title,cs.start_date,
       cont.first_name, cont.last_name, cont.email, city.name AS city, countries.name AS country,
	   r.name AS role, days.day, room.location, room.capacity, ls.start_time, ls.end_time   
FROM courses AS c
LEFT JOIN courses_schedule AS cs on c.id = cs.course_id
LEFT JOIN participants AS p on cs.id = p.course_schedule_id
LEFT JOIN roles AS r on r.id = p.role_id
LEFT JOIN contacts AS cont on cont.id = p.contact_id
LEFT JOIN cities AS city on city.id = cont.city_id
LEFT JOIN countries on countries.id = city.country_id
LEFT JOIN lectures_schedule AS ls on ls.course_schedule_id=cs.id
LEFT JOIN classrooms AS room on room.id = ls.classroom_id
LEFT JOIN week_days AS days on days.id = ls.week_day_id
ORDER BY c.id,cont.id,cs.start_date; 

-- Овде ми се поврзани информациите од 13 табели (недостигаат трите за плаќањата)

SELECT c.id, c.title,cs.start_date,
       cont.first_name, cont.last_name, cont.email, city.name AS city, countries.name AS country,
	   u.username, u.password,r.name AS role, 
	   days.day, room.location, room.capacity, ls.start_time, ls.end_time,
	   pay.payment_date, pay.amount, pm.name AS payment_method 
FROM courses AS c
LEFT JOIN courses_schedule AS cs on c.id = cs.course_id
LEFT JOIN participants AS p on cs.id = p.course_schedule_id
LEFT JOIN roles AS r on r.id = p.role_id
LEFT JOIN contacts AS cont on cont.id = p.contact_id
LEFT JOIN users AS u on u.contact_id = cont.id
LEFT JOIN cities AS city on city.id = cont.city_id
LEFT JOIN countries on countries.id = city.country_id
LEFT JOIN lectures_schedule AS ls on ls.course_schedule_id=cs.id
LEFT JOIN classrooms AS room on room.id = ls.classroom_id
LEFT JOIN week_days AS days on days.id = ls.week_day_id
LEFT JOIN payments AS pay on pay.contacts_id = cont.id
LEFT JOIN payment_methods AS pm on pm.id = pay.payment_method_id
ORDER BY c.id,cont.id,cs.start_date;

-- JOIN на сите 16 табели

SELECT c.id, c.title,cs.start_date,
       cont.first_name, cont.last_name, cont.email, city.name AS city, countries.name AS country,
	   u.username, u.password,r.name AS role, 
	   days.day, room.location, room.capacity, ls.start_time, ls.end_time,
	   pay.payment_date, pay.amount, pm.name AS payment_method,
	   ex.name AS exam_name, ex.questions,ex.duration_min, ep.name AS exam_place,
	   te.time_taken,te.result_percents, 
	   te.correct_quest, te.passed 
FROM courses AS c
LEFT JOIN courses_schedule AS cs on c.id = cs.course_id
LEFT JOIN participants AS p on cs.id = p.course_schedule_id
LEFT JOIN roles AS r on r.id = p.role_id
LEFT JOIN contacts AS cont on cont.id = p.contact_id
LEFT JOIN users AS u on u.contact_id = cont.id
LEFT JOIN cities AS city on city.id = cont.city_id
LEFT JOIN countries on countries.id = city.country_id
LEFT JOIN lectures_schedule AS ls on ls.course_schedule_id=cs.id
LEFT JOIN classrooms AS room on room.id = ls.classroom_id
LEFT JOIN week_days AS days on days.id = ls.week_day_id
LEFT JOIN payments AS pay on pay.contacts_id = cont.id
LEFT JOIN payment_methods AS pm on pm.id = pay.payment_method_id
LEFT JOIN taken_exams AS te on te.contact_id = cont.id
LEFT JOIN exams AS ex on ex.id = te.exam_id
LEFT JOIN exam_places AS ep on ep.id = ex.exam_place_id
ORDER BY c.id,cont.id,cs.start_date; 


